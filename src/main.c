#include "tests.h"
#include <stdio.h>

int main(void) {
  printf("Starting the tests!!!\n\n");
  if (test_1() && test_2() && test_3() && test_4()) {
    puts("Success");
    return 0;
  } else {
    puts("Damn. Failed");
    return 1;
  }
}
