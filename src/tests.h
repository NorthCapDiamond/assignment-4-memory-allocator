#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H
#define _DEFAULT_SOURCE
#define _DEFAULT_SOURCE

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "mem.h"
#include "mem_internals.h"
bool test_1();
bool test_2();
bool test_3();
bool test_4();

#endif 
