#include "tests.h"

bool test_1() {

  printf("test1 successful memory acquisition\n");

  void *heap = heap_init(100);
  if (!heap) {
    fprintf(stderr, "Error: The heap has not been initialized\n");
    return false;
  }

  debug_heap(stdout, heap);
  void *alloc = _malloc(10);

  if (!alloc) {
    fprintf(stderr, "Error: Something went wrong with malloc\n");
    return false;
  }

  debug_heap(stdout, heap);
  _free(alloc);

  munmap(HEAP_START, 8192);

  printf("test 1 passed!\n\n");
  return true;
}

bool test_2() {

  printf("test 2. Freeing one block from several allocated / Freeing two blocks from several allocated .\n");

  void *heap = heap_init(100);

  if (!heap) {
    fprintf(stderr, "Error: The heap has not been initialized\n");
    return false;
  }

  debug_heap(stdout, heap);

  void *alloc_1 = _malloc(10);
  void *alloc_2 = _malloc(20);

  if (!alloc_1 || !alloc_2) {
    fprintf(stderr, "Error: Something went wrong with malloc\n");
    return false;
  }

  debug_heap(stdout, heap);
  _free(alloc_1);
  debug_heap(stdout, heap);

  munmap(HEAP_START, 8192);

  printf("Freeing two blocks from several allocated blocks.\n");

  heap = heap_init(100);

  if (!heap) {
    fprintf(stderr, "Error: The heap has not been initialized\n");
    return false;
  }

  debug_heap(stdout, heap);

  alloc_1 = _malloc(10);
  alloc_2 = _malloc(20);
  void *alloc_3 = _malloc(30);

  if (!alloc_1 || !alloc_2 || !alloc_3) {
    fprintf(stderr, "Error: The heap has not been initialized\n");
    return false;
  }

  debug_heap(stdout, heap);
  _free(alloc_1);
  debug_heap(stdout, heap);
  _free(alloc_3);
  debug_heap(stdout, heap);

  munmap(HEAP_START, 8192);
  printf("test 2 passed\n\n");
  return true;
}

bool test_3() {
  printf("test 3, The new memory region expands the old one.\n");

  void *heap = heap_init(100);

  debug_heap(stdout, heap);

  void *alloc_1 = _malloc(10000);

  if (!alloc_1) {
    fprintf(stderr, "Error: Something went wrong with malloc\n");
    return false;
  }

  debug_heap(stdout, heap);
  _free(alloc_1);

  munmap(heap, 20480);
  printf("test 3 passed!\n\n");
  return true;
}

bool test_4() {
  printf("test 4. The old memory region cannot be expanded due to a different "
         "allocated address range, the new region is allocated elsewhere.\n");

  void *heap = heap_init(100);

  void *map = mmap(HEAP_START + 8192, 4096, PROT_READ | PROT_WRITE,
                   MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

  debug_heap(stdout, heap);

  void *alloc_1 = _malloc(10000);

  if (!alloc_1) {
    fprintf(stderr, "Error: Something went wrong with malloc\n");
    return false;
  }

  debug_heap(stdout, heap);

  _free(alloc_1);
  debug_heap(stdout, heap);

  munmap(map, 4096);
  printf("test 4 passed \n\n");
  return true;
}


